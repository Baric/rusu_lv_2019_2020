def findLargestNumber(numbers):
    highestValue = numbers[0]
    for number in numbers:
        if number > highestValue:
            highestValue = number
    return highestValue

def findLowestNumber(numbers):
    lowestValue = numbers[0]
    for number in numbers:
        if number < lowestValue:
            lowestValue = number
    return lowestValue

numbers = []
i=0
sum=0

while True:
    number = input("Input a number: ")
    if number == "Done":
        break
    try:
        value = int(number)
        numbers.append(value)
        i = i + 1
    except ValueError:
        try:
            value = float(number)
            numbers.append(value)
            i = i + 1
        except ValueError:
            print("This is not a number")

for number in numbers:
    sum = sum + number
averageValue = sum/i

highestValue = findLargestNumber(numbers)
lowestValue = findLowestNumber(numbers)

print("List of numbers: " ,i)
print("Average value: " ,averageValue) 
print("Largest value: " ,highestValue) 
print("Lowest value: " ,lowestValue)
