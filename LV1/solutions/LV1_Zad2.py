while True:
    try:
        gradeValue = float(input("Enter grade value: "))
        if gradeValue <= 1.0 and gradeValue >= 0:
            break
        else:
            print("Number must be in range from 0.0 to 1.0.")
    except ValueError:
        print("Given input is not a number.")
   
if gradeValue >= 0.9:
    print("Your final grade is: A")
elif gradeValue >= 0.8:
    print("Your final grade is: B")
elif gradeValue >= 0.7:
    print("Your final grade is: C")
elif gradeValue >= 0.6:
    print("Your final grade is: D")
else: 
    print("Your final grade is: F")