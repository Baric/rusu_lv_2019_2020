def isHostInDictionary(host, Dict):
    return Dict.get(host, 0)

def findHostname(email):
    host = 0
    hostname = ""
    email1 = []

    for letter in email:
        email1.append(letter)
    for i in range(0, len(email1)):
        if host == 1:
            hostname = hostname + email1[i]
        else: 
            if email1[i] == "@":
                host = 1
    return hostname             

emails = []
Dict = dict()
Hosts = []
fileName = input("Enter file name: ")
fhand = open(fileName)

for line in fhand:
    line = line.rstrip()
    if line.startswith('From'):
        words = line.split()
        for word in words:
            if "@" in word:
                emails.append(word)

for email in emails:
    Hosts.append(findHostname(email))

for host in Hosts:
    appearanceCounter = 0
    if(not(isHostInDictionary(host, Dict))):
        hostNameToCount = host
        for host in Hosts:
            if host == hostNameToCount:
                appearanceCounter = appearanceCounter + 1
        Dict[hostNameToCount] = appearanceCounter

print(Dict)