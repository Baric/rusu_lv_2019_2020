import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 

cars = pd.read_csv("mtcars.csv")  
plt.rcParams["figure.figsize"] = (15,15)
plt.scatter(cars.mpg, cars.hp)

for i in range(len(cars.mpg)):
    plt.annotate(cars.wt[i], (cars.mpg[i], cars.hp[i]), xytext=(cars.mpg[i], cars.hp[i]))

print("Lowest consumption:", np.min(cars.mpg))
print("Avergae consumption:", np.average(cars.mpg))
print("Highest consumption:", np.max(cars.mpg))