import numpy as np
import matplotlib.pyplot as plt

population = []
heightInCm = []
male = []
female = []

population = np.random.randint(2, size = 30)
print()

for pep in population:
    if pep == 1:
        a = np.random.normal(180,7)
        heightInCm.append(a)
        male.append(a)
    else:
        b = np.random.normal(167,7)
        heightInCm.append(b)
        female.append(b)
        

maleHeight = 0
femaleHeight = 0

i=0
w=0
m=0

for pep in population:
    if pep == 1:
        maleHeight += heightInCm[i]
        m += 1
    else:
        femaleHeight += heightInCm[i]
        w += 1
    i += 1

meanMale = maleHeight/m
meanFemale = femaleHeight/w

figure = plt.figure()
ax = figure.add_subplot()

ax.plot(male, color='blue')
ax.axhline(np.average(male), color='black')

figure1 = plt.figure()
ax2 = figure1.add_subplot()

ax2.plot(female, color='red')
ax2.axhline(np.average(female), color='black')
