import numpy as np
import matplotlib.pyplot as plt

numbers = np.random.randint(1, 7, size = 100)
plt.hist(numbers, bins = 20)