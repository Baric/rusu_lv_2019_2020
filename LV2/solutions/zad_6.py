import matplotlib.pyplot as plt

import matplotlib.image as mpimg

import numpy as np

image = mpimg.imread('tiger.png')

imgplot = plt.imshow(image)

new_image = np.zeros(image.shape, image.dtype)

beta = 0.3

for y in range(image.shape[0]):
    for x in range(image.shape[1]):
        for c in range(image.shape[2]):
            new_image[y,x,c] = np.clip(image[y,x,c] + beta, 0, 1)
            
imgplot = plt.imshow(new_image)